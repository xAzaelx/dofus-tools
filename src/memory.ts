import * as zlib from 'zlib'
import * as fs from 'fs'
import * as d2o from '../parsed/data/d2o/interfaces'
import { D2O_OUT, I18N_OUT } from './constants'
import { I18nRef } from './parser/game-files/i18n.format'

const LANG = 'de'

const i18n_compress = fs.readFileSync(`${I18N_OUT}/i18n_${LANG}.json.gz`)
const i18n_buffer = zlib.gunzipSync(i18n_compress)
const i18n_content: { [key: string]: string } = JSON.parse(i18n_buffer.toString('utf-8'))

const d2o_memory: d2o.D2oFileTypes = {}

export function getData<K extends keyof d2o.D2oFileTypes>(
  file: K
): NonNullable<d2o.D2oFileTypes[K]> {
  if (d2o_memory[file] !== undefined) {
    return d2o_memory[file] as any
  } else {
    const content = fs.readFileSync(`${D2O_OUT}/json_gzip/${file}.json.gz`)
    const uncompress = zlib.gunzipSync(content)
    const parsed = JSON.parse(uncompress.toString('utf-8'))
    d2o_memory[file] = parsed
    return parsed
  }
}
export const getI18n = (ref: I18nRef) => i18n_content[ref.key] || 'Text not found'
export const getI18nByKey = (key: number) => i18n_content[key] || 'Text not found'
