const Cap = require('cap').Cap
const decoders = require('cap').decoders
const chalk = require('chalk')
import dedent from 'dedent-js'
import * as text from './utils/logTexts'
import { secInit } from './utils/logTexts'

const TAG = chalk.yellow('[PCAP] ')
const PROTOCOL = decoders.PROTOCOL

let fragmentedBuffer: Buffer | undefined = undefined

export const startCapturing = (callback: (b: Buffer) => void) => {
  const c = new Cap()
  const device = Cap.findDevice('192.168.0.147')
  const filterOut = 'tcp and dst port 5555' // outgoing to dofus server
  const filterIn = 'tcp and src port 5555' // incoming from dofus server
  const bufSize = 10 * 1024 * 1024
  const buffer = Buffer.alloc(65535)

  const linkType = c.open(device, filterIn, bufSize, buffer)

  c.setMinBytes && c.setMinBytes(0)

  console.log(dedent`
      ${chalk.green('|-----------------------------------------------')}
      ${secInit} Starting to capture packages on IP ${chalk.blue(device)}
      ${secInit} Using filter ${chalk.blue(filterIn)}
      ${chalk.green('|-----------------------------------------------')}
    `)

  c.on('packet', function (nbytes: any, trunc: any) {
    if (linkType === 'ETHERNET') {
      const ethHeader = decoders.Ethernet(buffer)

      if (ethHeader.info.type === PROTOCOL.ETHERNET.IPV4) {
        const ipHeader = decoders.IPV4(buffer, ethHeader.offset)

        if (ipHeader.info.protocol === PROTOCOL.IP.TCP) {
          let datalen = ipHeader.info.totallen - ipHeader.hdrlen
          const tcpHeader = decoders.TCP(buffer, ipHeader.offset)

          datalen -= tcpHeader.hdrlen

          const src_dst = `${ipHeader.info.srcaddr}:${tcpHeader.info.srcport} -> ${ipHeader.info.dstaddr}:${tcpHeader.info.dstport}`
          console.log(chalk.yellow(`|-- PCAP ---- ${nbytes} bytes --------------------`))
          const dataBuffer = buffer.subarray(tcpHeader.offset, tcpHeader.offset + datalen)
          if (nbytes === 1514) {
            let combinedBuffer =
              fragmentedBuffer !== undefined
                ? Buffer.concat([fragmentedBuffer, dataBuffer])
                : Buffer.from(dataBuffer)
            fragmentedBuffer = combinedBuffer
          } else {
            if (fragmentedBuffer) {
              const finishedBuffer = Buffer.concat([fragmentedBuffer, dataBuffer])
              fragmentedBuffer = undefined
              console.log(chalk.yellow('|'), finishedBuffer)
              callback(finishedBuffer)
            } else {
              console.log(chalk.yellow('|'), dataBuffer)
              callback(dataBuffer)
            }
          }
        } else if (ipHeader.info.protocol === PROTOCOL.IP.UDP) {
          text.ignoringUDP()
        } else {
          text.unsupportedIPv4Protocol(PROTOCOL.IP[ipHeader.info.protocol])
        }
      } else {
        text.unsupportedEthertype(PROTOCOL.ETHERNET[ethHeader.info.type])
      }
    }
    // log('----------------------------------------------------')
  })
}
