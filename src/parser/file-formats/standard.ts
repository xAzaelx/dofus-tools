import * as buffer from '../../utils/buffer'
import { FormatSpec } from '../../utils/buffer'

export interface StringFormat {
  length: number
  string: number
}
export const StringFormatSpec = (length = 2): FormatSpec<StringFormat> => {
  return [
    {
      type: 'fixed_length',
      name: 'length',
      length,
      transform: buffer.toNumber,
    },
    {
      type: 'variable_length',
      name: 'string',
      transform: buffer.toText,
      parameter: {
        off: 'length',
        transform: buffer.toNumber,
      },
    },
  ]
}

export interface VectorFormat {
  length: number
  array: number[]
}
export const VectorFormatSpec: (typeSpec: buffer.FormatEntries<any>) => FormatSpec<VectorFormat> = (
  typeSpec
) => [
  {
    type: 'fixed_length',
    name: 'length',
    length: 4,
    transform: buffer.toNumber,
  },
  {
    type: 'variable_array',
    name: 'array',
    transform: (obj: any) => obj[typeSpec.name],
    format: [typeSpec],
    parameter: {
      off: 'length',
      transform: buffer.toNumber,
    },
  },
]

export function describeChunkString<Format>(
  name: keyof Format,
  lengthBytes = 2,
  debug?: boolean
): buffer.ChunkEntry<Format, StringFormat> {
  return {
    type: 'chunk',
    name,
    debug: debug || false,
    format: StringFormatSpec(lengthBytes),
    transform: (stringFormat) => stringFormat.string,
  }
}
export function describeFixedString<Format>(
  name: keyof Format,
  length: number,
  debug?: boolean
): buffer.FixedEntry<Format> {
  return {
    type: 'fixed_length',
    name,
    length,
    transform: buffer.toText,
  }
}
export function describeNumber<Format>(
  name: keyof Format,
  length: number,
  debug?: boolean
): buffer.FixedEntry<Format> {
  return {
    type: 'fixed_length',
    name,
    debug: debug || false,
    length,
    transform: buffer.toNumber,
  }
}
export function describeInt<Format>(
  name: keyof Format,
  debug?: boolean
): buffer.FixedEntry<Format> {
  return {
    type: 'fixed_length',
    name,
    debug: debug || false,
    length: 4,
    transform: buffer.toNumber,
  }
}
export function describeShort<Format>(
  name: keyof Format,
  debug?: boolean
): buffer.FixedEntry<Format> {
  return {
    type: 'fixed_length',
    name,
    debug: debug || false,
    length: 2,
    transform: buffer.toNumber,
  }
}
export function describeDouble<Format>(
  name: keyof Format,
  debug?: boolean
): buffer.FixedEntry<Format> {
  return {
    type: 'fixed_length',
    name,
    debug: debug || false,
    length: 8,
    transform: buffer.toNumber,
  }
}
export function describeBool<Format>(
  name: keyof Format,
  debug?: boolean
): buffer.FixedEntry<Format> {
  return {
    type: 'fixed_length',
    name,
    debug: debug || false,
    length: 1,
    transform: (b: Buffer) => buffer.toNumber(b) >= 1,
  }
}

export interface ArrayFormat<T> {
  arrayLength: number
  array: T[]
}
export function describeArray<Format, EntryFormat>(
  name: keyof Format,
  format: FormatSpec<EntryFormat>,
  length: 'short' | 'int',
  transform?: (obj: EntryFormat) => any,
  debug?: boolean
): buffer.ChunkEntry<Format, ArrayFormat<EntryFormat>> {
  return {
    type: 'chunk',
    name,
    debug: debug || false,
    format: [
      length === 'int' ? describeInt('arrayLength') : describeShort('arrayLength'),
      {
        type: 'variable_array',
        name: 'array',
        transform,
        format,
        parameter: {
          off: 'arrayLength',
          transform: buffer.toNumber,
        },
      },
    ],
    transform: (arrayDef) => arrayDef.array,
  }
}
export function describeByteArray<Format, EntryFormat>(
  name: keyof Format,
  format: FormatSpec<EntryFormat>,
  debug?: boolean
): buffer.ChunkEntry<Format, ArrayFormat<EntryFormat>> {
  return {
    type: 'chunk',
    name,
    debug: debug || false,
    format: [
      describeInt('arrayLength'),
      {
        type: 'variable_length',
        name: 'array',
        transform: (b: Buffer) => buffer.recurseNamedChunks(b, format),
        parameter: {
          off: 'arrayLength',
          transform: buffer.toNumber,
        },
      },
    ],
    transform: (arrayDef) => arrayDef.array,
  }
}
export function describeBuffer<Format>(
  name: keyof Format,
  transform?: (b: Buffer) => number,
  debug?: boolean
): buffer.ChunkEntry<Format, { length: number; buffer: Buffer }> {
  return {
    type: 'chunk',
    name,
    debug: debug || false,
    format: [
      describeInt('length'),
      {
        type: 'variable_length',
        name: 'buffer',
        transform: (b: Buffer) => b,
        parameter: {
          off: 'length',
          transform: transform || buffer.toNumber,
        },
      },
    ],
    transform: (arrayDef) => arrayDef.buffer,
  }
}
