import * as buffer from '../../utils/buffer'
import { FormatSpec } from '../../utils/buffer'
import {
  describeChunkString,
  describeInt,
  describeArray,
  describeFixedString,
  describeByteArray,
  describeBuffer,
} from '../file-formats/standard'

export enum D2OFieldType {
  INT = -1,
  BOOLEAN = -2,
  STRING = -3,
  DOUBLE = -4,
  I18N = -5,
  UINT = -6,
  VECTOR = -99,
}

export interface PointerDescriptor {
  dataIdRef: number
  dataStartPointer: number
}
export const PointerDescriptorSpec: FormatSpec<PointerDescriptor> = [
  describeInt('dataIdRef'),
  describeInt('dataStartPointer'),
]

export interface FieldDescriptor {
  name: string
  type: D2OFieldType
  vector?: FieldDescriptor
}
export const FieldDescriptorSpec: FormatSpec<FieldDescriptor> = [
  describeChunkString('name'),
  describeInt('type'),
]
FieldDescriptorSpec.push({
  type: 'chunk',
  format: FieldDescriptorSpec,
  name: 'vector',
  condition: {
    on: 'type',
    check: (b: Buffer) => buffer.toNumber(b) === D2OFieldType.VECTOR,
  },
})

export interface MemberDescriptor {
  memberId: number
  name: string
  package: string
  fields: FieldDescriptor[]
}
export const memberDescriptorSpec: FormatSpec<MemberDescriptor> = [
  describeInt('memberId'),
  describeChunkString('name'),
  describeChunkString('package'),
  describeArray('fields', FieldDescriptorSpec, 'int'),
]

export interface EndListDescriptor {
  fieldName: string
  dataStartPointer: number
  fieldType: D2OFieldType
  maybeFileRef: number
}
export const EndListSpec: FormatSpec<EndListDescriptor> = [
  describeChunkString('fieldName'),
  describeInt('dataStartPointer'),
  describeInt('fieldType'),
  describeInt('maybeFileRef'),
]

export interface D2OFormat {
  d2oHeader: string
  dataList: Buffer
  pointerList: PointerDescriptor[]
  memberList: MemberDescriptor[]
  endListDesc: EndListDescriptor[]
  endList: Buffer
}

export const d2oFormatSpec: FormatSpec<D2OFormat> = [
  describeFixedString('d2oHeader', 3),
  describeBuffer('dataList', (b: Buffer) => buffer.toNumber(b) - 7),
  describeByteArray('pointerList', PointerDescriptorSpec),
  describeArray('memberList', memberDescriptorSpec, 'int'),
  describeByteArray('endListDesc', EndListSpec),
  describeBuffer('endList'),
]
