import * as fs from 'fs'
import dedent from 'dedent-js'
import chalk from 'chalk'
import { DOFUS_FOLDER, DATA_FOLDER, ARCHIVE_FOLDER, PARSED_FOLDER } from '../../constants'
import { secInit } from '../../utils/logTexts'
import { parseI18nFiles } from './i18n.parser'
import { parseD2oFiles } from './d2o.parser'
import { getData, getI18n } from '../../memory'

export const parseGameFiles = () => {
  const gameVersion = fs
    .readFileSync(DOFUS_FOLDER + '/VERSION')
    .toString()
    .trim()

  console.log(dedent`
    ${chalk.green('|-----------------------------------------------')}
    ${secInit} ${chalk.bold('GAME_VERSION:')} ${chalk.redBright(gameVersion)}
  `)
  if (fs.existsSync(DATA_FOLDER)) {
    try {
      const dataVersion = fs
        .readFileSync(DATA_FOLDER + '/VERSION')
        .toString()
        .trim()
      if (dataVersion === gameVersion) {
        console.log(dedent`
              ${secInit} Parsed Data for this Version existing, no need to parse.
              ${chalk.green('|-----------------------------------------------')}
            `)
        return
      } else {
        fs.renameSync(DATA_FOLDER, ARCHIVE_FOLDER + '/data_' + dataVersion)
      }
    } catch (e) {
      console.log(e)
    }
  }
  console.log(dedent`
      ${secInit} No parsed data, Begin Parsing ....
      ${chalk.green('|-----------------------------------------------')}
    `)
  fs.mkdirSync(DATA_FOLDER, { recursive: true })
  fs.writeFileSync(`${DATA_FOLDER}/VERSION`, gameVersion)
  parseI18nFiles()
  parseD2oFiles()
}

