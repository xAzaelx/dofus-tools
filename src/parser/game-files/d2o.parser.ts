import * as zlib from 'zlib'
import * as fs from 'fs'
import dedent from 'dedent-js'
import chalk from 'chalk'
import * as buffer from '../../utils/buffer'
import {
  D2OFormat,
  d2oFormatSpec,
  D2OFieldType,
  FieldDescriptor,
  MemberDescriptor,
} from './d2o.format'
import { logPath, secInit, check, cross, warn, logger } from '../../utils/logTexts'
import {
  describeInt,
  describeBool,
  describeChunkString,
  describeDouble,
  describeArray,
} from '../file-formats/standard'
import { performance } from 'perf_hooks'
import d2oIdMapping from './d2o-id-mapping.json'
import { JsonObject } from '../../utils/types'
import { prettyJson, jsonToInterfaceString } from '../../utils/misc'
import { D2O_OUT, D2O_IN } from '../../constants'

type SpecsAndFormats = {
  [key: string]: { spec: buffer.FormatSpec<any>; format: MemberDescriptor }
}
type RequiredParams = {
  D2O_IN: string
  D2O_OUT: string
}

export const parseD2oFiles = () => {
  const ID_MAPPING = d2oIdMapping as { [key: string]: string }
  const FIELDS_OUT = D2O_OUT + `/fields`
  const JSON_OUT = D2O_OUT + `/json_gzip`
  const INTERFACES_FILE = D2O_OUT + `/interfaces.ts`

  let mainTypeAccumulator: { [key: string]: { name: string; type: string } } = {}
  let interfaceAccumulator: { [key: string]: Array<{ name: string; iface: any }> } = {}

  const generateInterfaceFieldBasedOnType = (
    field: FieldDescriptor,
    lookup: (field: FieldDescriptor) => string
  ): string | JsonObject => {
    switch (field.type) {
      case D2OFieldType.INT:
      case D2OFieldType.UINT:
      case D2OFieldType.DOUBLE:
        return `number`
      case D2OFieldType.I18N:
        return `I18nRef`
      case D2OFieldType.BOOLEAN:
        return `boolean`
      case D2OFieldType.STRING:
        return `string`
      case D2OFieldType.VECTOR:
        if (field.vector) {
          const vectorType = generateInterfaceFieldBasedOnType(field.vector, lookup)
          return `Array<${vectorType}>`
        }
      default:
        return lookup(field)
    }
  }

  const generateSpecBasedOnType = (
    field: FieldDescriptor,
    lookup: (b: Buffer) => buffer.FormatSpec<any> | undefined
  ): buffer.FormatEntries<any> => {
    switch (field.type) {
      case D2OFieldType.INT:
      case D2OFieldType.UINT:
        return describeInt(field.name)
      case D2OFieldType.I18N:
        return {
          type: 'fixed_length',
          length: 4,
          name: field.name,
          transform: (b: Buffer) => ({ key: buffer.toNumber(b) }),
        }
      case D2OFieldType.BOOLEAN:
        return describeBool(field.name)
      case D2OFieldType.STRING:
        return describeChunkString(field.name)
      case D2OFieldType.DOUBLE:
        return describeDouble(field.name)
      case D2OFieldType.VECTOR:
        if (field.vector) {
          const vectorSpec = generateSpecBasedOnType(field.vector, lookup)
          return describeArray(field.name, [vectorSpec], 'int', (obj) => obj[vectorSpec.name])
        }
      default:
        return {
          type: 'chunk',
          name: field.name,
          transform: (obj) => ({ memberId: obj.memberId, ...obj.innerMember }),
          format: [
            describeInt('memberId'),
            {
              type: 'format_lookup',
              name: 'innerMember',
              parameter: {
                off: 'memberId',
                lookup,
              },
            },
          ],
        }
    }
  }

  const generateTsInterfaces = (specs: SpecsAndFormats, filename: string) => {
    const interfaceName = (name: string) =>
      name.startsWith(filename) || filename.startsWith(name) ? name : filename + name
    const lookup = (field: FieldDescriptor) => interfaceName(specs[field.type].format.name)
    const memberInterfaces: any[] = []
    Object.keys(specs).forEach((key) => {
      const generatedInterface = specs[key].format.fields.reduce((acc, field) => {
        acc[field.name] = generateInterfaceFieldBasedOnType(field, lookup)
        return acc
      }, {} as JsonObject)
      memberInterfaces.push({ name: specs[key].format.name, iface: generatedInterface })
    })
    interfaceAccumulator[filename] = memberInterfaces
  }

  const parseD2oContent = (b: Buffer, filename: string) => {
    const [chunks] = buffer.namedChunks<D2OFormat>(b, d2oFormatSpec)

    // Generate Field Specs
    let memberSpecs: SpecsAndFormats = {}
    const lookup = (b: Buffer) => {
      const memberId = buffer.toNumber(b)
      return memberSpecs[memberId] ? memberSpecs[memberId].spec : undefined
    }
    chunks.memberList.forEach((member) => {
      memberSpecs[member.memberId] = {
        format: member,
        spec: member.fields.map((field) => generateSpecBasedOnType(field, lookup)),
      }
    })
    fs.writeFileSync(`${FIELDS_OUT}/${filename}.json`, prettyJson(memberSpecs))
    generateTsInterfaces(memberSpecs, filename)

    // Parse D2O data
    let mainFileType: string = ''
    const dataArray = buffer.recurseNamedChunks<any>(
      chunks.dataList,
      [
        describeInt('memberId'),
        {
          type: 'format_lookup',
          name: 'outerMember',
          parameter: {
            off: 'memberId',
            lookup,
          },
        },
      ],
      (obj) => {
        if (mainFileType.length === 0) {
          mainFileType = memberSpecs[obj.memberId].format.name
        }
        return { memberId: obj.memberId, ...obj.outerMember }
      }
    )
    mainTypeAccumulator[filename] = {
      name: mainFileType,
      type: `{ [key: string]: ${mainFileType} }`,
    }
    const fileInterfaces = interfaceAccumulator[filename]
    interfaceAccumulator[filename] = fileInterfaces.map((member) => ({
      name: member.name === mainFileType ? mainFileType : filename + member.name,
      iface: member.iface,
    }))

    // Write parsed data to files
    const indexedById: { [key: string]: any } = {}
    const indexBy = ID_MAPPING[filename] || 'id'
    dataArray.forEach((data) => {
      const idKey = data[indexBy]
      if (idKey === undefined)
        throw new Error('Could not index data by an ID field. (Key used ' + indexBy + '")')
      indexedById[idKey] = data
    })
    const compress = zlib.gzipSync(prettyJson(indexedById))
    fs.writeFileSync(`${JSON_OUT}/${filename}.json.gz`, compress)
  }

  try {
    const files = fs.readdirSync(D2O_IN)
    console.log(dedent`
        ${chalk.green('|-----------------------------------------------')}
        ${secInit} Parsing D2O Files in folder ${logPath(D2O_IN)}
        ${secInit} Write output to:
        ${secInit} \t JSON: ${logPath(JSON_OUT)}
        ${secInit} \t Fields: ${logPath(FIELDS_OUT)}
        ${secInit} \t Interfaces: ${logPath(INTERFACES_FILE)}
        ${chalk.green('|-----------------------------------------------')}
      `)
    let t0 = performance.now()
    let success = 0
    let fail = 0
    let ignore = 0
    let tsInterfaceFile = dedent`
        interface I18nRef {
            key: number
        }`

    fs.mkdirSync(FIELDS_OUT, { recursive: true })
    fs.mkdirSync(JSON_OUT, { recursive: true })
    fs.writeFileSync(INTERFACES_FILE, tsInterfaceFile)

    files.forEach((file) => {
      if (file.endsWith('.d2o')) {
        const filename = file.replace('.d2o', '')
        try {
          const filecontent = fs.readFileSync(D2O_IN + file)
          parseD2oContent(filecontent, filename)
          success++
          console.log(`${secInit} ${check} ${chalk.bold(file)}`)
        } catch (e) {
          fail++
          logger.error(e)
          console.log(
            `${secInit} ${cross} ${chalk.bold(file)}: ${chalk.red('Additional logging above')}`
          )
        }
      } else {
        ignore++
        console.log(
          `${secInit} ${warn} ${chalk.bold(file)}: ${chalk.yellow('Ignored, no D2O File')}`
        )
      }
    })

    // Write interfaces to file
    Object.keys(interfaceAccumulator).forEach((file) =>
      interfaceAccumulator[file].forEach(({ name, iface }) => {
        const memberInterface = jsonToInterfaceString(name, iface)
        fs.appendFileSync(INTERFACES_FILE, '\n\n' + memberInterface)
      })
    )

    let interfaceString = '\n\nexport interface D2oFileTypes { \n'
    Object.keys(mainTypeAccumulator).forEach((file) => {
      const { name, type } = mainTypeAccumulator[file]
      interfaceString += `\t${file}?: ${type}\n`
    })
    interfaceString += '}\n\nexport type D2oFiles = keyof D2oFileTypes'

    fs.appendFileSync(INTERFACES_FILE, interfaceString)

    var t1 = performance.now()
    console.log(dedent`
        ${chalk.green('|-----------------------------------------------')}
        ${secInit} Parsing ${chalk.greenBright(files.length)} files took ${chalk.greenBright(
      Math.round(t1 - t0) / 1000
    )} seconds
        ${secInit} ${check}  ${success} file${success === 1 ? '' : 's'} successful
        ${secInit} ${warn}  ${ignore} file${ignore === 1 ? '' : 's'} ignored     
        ${secInit} ${cross}  ${fail} file${fail === 1 ? '' : 's'} failed       
        ${chalk.green('|-----------------------------------------------\n')}
      `)
  } catch (e) {
    return console.log(chalk.redBright('Unable to scan directory: ' + e))
  }
}
