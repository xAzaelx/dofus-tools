import * as fs from 'fs'
import * as zlib from 'zlib'
import dedent from 'dedent-js'
import chalk from 'chalk'
import * as buffer from '../../utils/buffer'
import { logPath, secInit, check, cross, warn } from '../../utils/logTexts'
import { performance } from 'perf_hooks'
import { i18nFileFormatSpec, I18nFileFormat, textFormatSpec, TextFormat } from './i18n.format'
import { prettyJson } from '../../utils/misc'
import { I18N_OUT, I18N_IN } from '../../constants'

type RequiredParams = {
  I18N_IN: string
  I18N_OUT: string
}

export const parseI18nFiles = async () => {
  const parseI18nContent = (b: Buffer, filename: string) => {
    const [chunks] = buffer.namedChunks<I18nFileFormat>(b, i18nFileFormatSpec)

    const textMapping = {} as any
    for (let i = 0; i < chunks.textPointerList.length; i++) {
      let { textId, textStart } = chunks.textPointerList[i]
      let restBuffer = b.slice(textStart, b.length)
      let [chunk] = buffer.namedChunks<TextFormat>(restBuffer, textFormatSpec)
      textMapping[textId] = chunk.text
    }

    const compress = zlib.gzipSync(prettyJson(textMapping))
    fs.mkdirSync(`${I18N_OUT}`, { recursive: true })
    fs.writeFileSync(`${I18N_OUT}/${filename}.json.gz`, compress)
  }

  try {
    const files = fs.readdirSync(I18N_IN)
    console.log(dedent`
        ${chalk.green('|-----------------------------------------------')}
        ${secInit} Parsing I18N Files in folder ${logPath(I18N_IN)}
        ${secInit} Write output to ${logPath(I18N_OUT)}
        ${chalk.green('|-----------------------------------------------')}
      `)

    var t0 = performance.now()
    let success = 0
    let fail = 0
    let ignore = 0
    files.forEach((file) => {
      if (file.endsWith('.d2i')) {
        const filename = file.replace('.d2i', '')
        try {
          const filecontent = fs.readFileSync(I18N_IN + file)
          parseI18nContent(filecontent, filename)
          console.log(`${secInit} ${check} ${chalk.bold(file)}`)
          success++
        } catch (e) {
          fail++
          console.log(
            `${secInit} ${cross} ${chalk.bold(file)}: ${chalk.red('Additional logging above')}`
          )
        }
      } else {
        ignore++
        console.log(
          `${secInit} ${warn} ${chalk.bold(file)}: ${chalk.yellow('Ignored, no D2I File')}`
        )
      }
    })
    var t1 = performance.now()
    console.log(dedent`
        ${chalk.green('|-----------------------------------------------')}
        ${secInit} Parsing ${chalk.greenBright(files.length)} files took ${chalk.greenBright(
      Math.round(t1 - t0) / 1000
    )} seconds
        ${secInit} ${check}  ${success} file${success === 1 ? '' : 's'} successful
        ${secInit} ${warn}  ${ignore} file${ignore === 1 ? '' : 's'} ignored     
        ${secInit} ${cross}  ${fail} file${fail === 1 ? '' : 's'} failed       
        ${chalk.green('|-----------------------------------------------\n')}
      `)
  } catch (e) {
    return console.log(chalk.redBright('Unable to scan directory: ' + e))
  }
}
