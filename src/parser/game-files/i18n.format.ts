import * as buffer from '../../utils/buffer'
import { FormatSpec } from '../../utils/buffer'

export interface I18nRef {
  key: number
}

export interface TextFormat {
  length: number
  text: string
}
export const textFormatSpec: FormatSpec<TextFormat> = [
  {
    type: 'fixed_length',
    name: 'length',
    length: 2,
    transform: buffer.toNumber,
  },
  {
    type: 'variable_length',
    name: 'text',
    transform: buffer.toText,
    parameter: { off: 'length', transform: buffer.toNumber },
  },
]
export interface TextPointerFormat {
  textId: number
  diacriticFlag: boolean
  textStart: number
  diacriticStart?: number
}
export const textPointerFormatSpec: FormatSpec<TextPointerFormat> = [
  {
    type: 'fixed_length',
    name: 'textId',
    length: 4,
    transform: buffer.toNumber,
  },
  {
    type: 'fixed_length',
    name: 'diacriticFlag',
    length: 1,
    transform: (b: Buffer) => buffer.toNumber(b) === 1,
  },
  {
    type: 'fixed_length',
    name: 'textStart',
    length: 4,
    transform: buffer.toNumber,
  },
  {
    type: 'fixed_length',
    length: 4,
    name: 'diacriticStart',
    transform: buffer.toNumber,
    condition: { on: 'diacriticFlag', check: (b: Buffer) => buffer.toNumber(b) === 1 },
  },
]
export interface UiTextFormat {
  length: number
  text: string
  maybeId: number
}
export const uiTextFormatSpec: FormatSpec<UiTextFormat> = [
  {
    type: 'fixed_length',
    name: 'length',
    length: 2,
    transform: buffer.toNumber,
  },
  {
    type: 'variable_length',
    name: 'text',
    transform: buffer.toText,
    parameter: { off: 'length', transform: buffer.toNumber },
  },
  {
    type: 'fixed_length',
    name: 'maybeId',
    length: 4,
    transform: buffer.toNumber,
  },
]
export interface TextIdFormat {
  stringId: number
}
export const textIdFormatSpec: FormatSpec<TextIdFormat> = [
  {
    type: 'fixed_length',
    name: 'stringId',
    length: 4,
    transform: buffer.toNumber,
  },
]
export interface I18nFileFormat {
  textListLength: number
  textList: Array<TextFormat>
  textPointerListLength: number
  textPointerList: Array<TextPointerFormat>
  uiTextListLength: number
  uiTextList: Array<UiTextFormat>
  textIdListLength: number
  textIdList: Array<TextIdFormat>
}
export const i18nFileFormatSpec: FormatSpec<I18nFileFormat> = [
  {
    type: 'fixed_length',
    name: 'textListLength',
    length: 4,
    transform: buffer.toNumber,
  },
  {
    type: 'variable_length',
    name: 'textList',
    transform: (b: Buffer) => buffer.recurseNamedChunks(b, textFormatSpec),
    parameter: { off: 'textListLength', transform: (b: Buffer) => buffer.toNumber(b) - 4 },
  },
  {
    type: 'fixed_length',
    name: 'textPointerListLength',
    length: 4,
    transform: buffer.toNumber,
  },
  {
    type: 'variable_length',
    name: 'textPointerList',
    transform: (b: Buffer) => buffer.recurseNamedChunks(b, textPointerFormatSpec),
    parameter: { off: 'textPointerListLength', transform: buffer.toNumber },
  },
  {
    type: 'fixed_length',
    name: 'uiTextListLength',
    length: 4,
    transform: buffer.toNumber,
  },
  {
    type: 'variable_length',
    name: 'uiTextList',
    transform: (b: Buffer) => buffer.recurseNamedChunks(b, uiTextFormatSpec),
    parameter: { off: 'uiTextListLength', transform: buffer.toNumber },
  },
  {
    type: 'fixed_length',
    name: 'textIdListLength',
    length: 4,
    transform: buffer.toNumber,
  },
  {
    type: 'variable_length',
    name: 'textIdList',
    transform: (b: Buffer) => buffer.recurseNamedChunks(b, textIdFormatSpec),
    parameter: { off: 'textIdListLength', transform: buffer.toNumber },
  },
]
