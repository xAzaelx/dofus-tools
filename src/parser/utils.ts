import { TakeUntilEntry } from '../utils/buffer'

/**
 * This is a weird one. You have to take the previous number in the buffer subtract 1,
 * multiply by 128 and then add the current number. This needs to be done from the back
 * to the front of the array. Acc starts with 1, so that 1-1 will be 0, and therefore
 * 0 will be the "first number"
 */
export const transformBufferToDofusValueFormat = (b: Buffer) =>
  b.reduceRight((acc, curr) => 128 * (acc - 1) + curr, 1)

export const describeAnkamaNumberFormat = (name: string): TakeUntilEntry<any> => ({
  name,
  type: 'take_until',
  stopCondition: (b: Buffer) => b[b.length - 1] < 0x80,
  transform: transformBufferToDofusValueFormat,
})
