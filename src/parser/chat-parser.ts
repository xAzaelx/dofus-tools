import * as buffer from '../utils/buffer'
import { Channel } from '../plugins/ingame-chat/channels'
import { formatMsg } from '../plugins/ingame-chat/format-msg'
import { MessageType } from './message.types'
import { itemListParser } from './item-parser'
import { describeShort, describeNumber, describeChunkString } from './file-formats/standard'
import { getData } from '../memory'

export interface ChatMessage {
  type: Buffer
  totalLength: number
  timestamp: number
  channel: Channel
  msg: string
  name: string
  fix1: Buffer
  fix2: Buffer
  fix3: Buffer
  unknown1: Buffer
  unknown2: Buffer
  unknown3: Buffer
}

const config: buffer.FormatSpec<ChatMessage> = [
  describeNumber('type', 2),
  describeNumber('totalLength', 1),
  describeNumber('channel', 1),
  { type: 'fixed_length', name: 'fix1', length: 1 },
  describeChunkString('msg', 1),
  describeNumber('timestamp', 4),
  { type: 'fixed_length', name: 'fix2', length: 2 },
  { type: 'fixed_length', name: 'unknown1', length: 8 },
  { type: 'fixed_length', name: 'fix3', length: 1 },
  { type: 'fixed_length', name: 'unknown2', length: 8 },
  describeChunkString('name', 1),
  { type: 'fixed_length', name: 'unknown3', length: 6 },
]

export const chatParser = (b: Buffer) => {
  const [chunks] = buffer.namedChunks<ChatMessage>(b, config)

  // @note Assumtion: 63b5 is Exo Item



  // if (concatHex(x.type) === MessageType.ItemChatMessage) {
  //   itemListParser(x.rest)
  // }

  console.log(formatMsg(chunks))
}
