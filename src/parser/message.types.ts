export enum MessageType {
  StandardChatMessage = 0x0dc5,
  ItemChatMessage = 0x0dcd,
  MessageFollowingType = 0x02c0,
  TODO_4IntThenMessage = 0x59c5,
  StorageMessage = 0x6171
}

// 0x02f5 ((Info) The message could not be sent. -> too much spam)
// 0x0c31 ((Important) Your message was not sent because it was the same as the previous one.)
