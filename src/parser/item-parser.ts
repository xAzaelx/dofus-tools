import { split } from '../utils/buffer'
import { bufferToInt } from '../utils/hex'
import { Channel } from '../plugins/ingame-chat/channels'
import { formatMsg } from '../plugins/ingame-chat/format-msg'
import { MessageType } from './message.types'

const SOI1 = 0x00 // First Byte of StartOfItem
const SOI2 = 0x3f // Second Byte of StartOfItem
const EOI = 0x01 // End Of Item

export interface ChatMessage {
  type: Buffer
  totalLength: number
  timestamp: number
  channel: Channel
  msgLength: number
  msg: string
  nameLength: number
  name: string
  null1: Buffer
  fix1: Buffer
  fix2: Buffer
  unknown1: Buffer
  unknown2: Buffer
  unknown3: Buffer
}

export const itemListParser = (buffer: Buffer) => {
  var [null1, rest] = split(buffer, 1)
  var [itemAmount, rest] = split(rest, 1)

  let itemArray = []
  for (let i = 0; i < rest.length; i++) {
    let marker = 0
    let curr = rest[i]

    if (curr === SOI2 && rest[i - 1] === SOI1 && rest[i - 2] === EOI) {
      let itemBuffer = rest.slice(marker, i)
      let item = itemParser(itemBuffer)
      itemArray.push(item)
      marker = i + -1
    }
  }

  if (itemArray.length !== itemAmount[0]) {
    console.error(
      'Could not parse itemlist correctly: ' + buffer.toString('hex')
    )
  }

  var [channel, rest] = split(rest, 1)
  var [null1, rest] = split(rest, 1)
  var [msgLen, rest] = split(rest, 1)
  var [msg, rest] = split(rest, msgLen[0])
  var [timestamp, rest] = split(rest, 4)
  var [fix1, rest] = split(rest, 2)
  var [unknown1, rest] = split(rest, 8)
  var [fix2, rest] = split(rest, 1)
  var [unknown2, rest] = split(rest, 8)
  var [nameLen, rest] = split(rest, 1)
  var [name, rest] = split(rest, nameLen[0])
  var [unknown3, rest] = split(rest, 6)

  if (bufferToInt(type) === MessageType.ItemChatMessage) {
  }

  const chatMessage: ChatMessage = {
    type,
    totalLength: totalLen[0],
    channel: channel[0],
    null1,
    msgLength: msgLen[0],
    msg: msg.toString('binary'),
    timestamp: bufferToInt(timestamp),
    fix1,
    unknown1,
    fix2,
    unknown2,
    nameLength: nameLen[0],
    name: name.toString('binary'),
    unknown3,
  }
  // console.log('chatParser -> chatMessage', chatMessage)
  console.log(formatMsg(chatMessage))
}

export const itemParser = (buffer: Buffer) => {
  // check for ISI and IEI
  if (
    buffer[0] === SOI1 &&
    buffer[1] === SOI2 &&
    buffer[buffer.length - 1] === EOI
  ) {
    var [trim_soi, rest] = split(buffer, 2)
    var [rest, trim_eoi] = split(rest, -1)

    let EOitemTypeId = buffer.indexOf(0x00)
    var [itemTypeId, rest] = split(rest, EOitemTypeId)
    var [trim_eoiti, rest] = split(rest, 1)
    var [statAmount, rest] = split(rest, 1)
    let statsArray
    let itemInstanceId
  } else {
    console.error(
      'Could not detect start and end indicators of item: ' +
        buffer.toString('hex')
    )
  }
}
