import * as buffer from '../utils/buffer'
import { Channel } from '../plugins/ingame-chat/channels'
import { formatMsg } from '../plugins/ingame-chat/format-msg'
import { MessageType } from './message.types'
import { itemListParser } from './item-parser'
import {
  describeShort,
  describeNumber,
  describeChunkString,
  describeArray,
} from './file-formats/standard'
import { getData } from '../memory'
import { describeAnkamaNumberFormat } from './utils'
import { lookup } from 'dns'
import { logger } from '../utils/logTexts'
import { accumulateSoulsFromChest } from '../plugins/eternal-harvest'

export interface ItemEffect {
  effectType: number
  itemEffects: any
}
export interface StorageItem {
  unknown1_003f: number
  itemTypeId: number
  effects: Array<ItemEffect>
  itemInstanceId: number
  unknown1: number
  stack: number
}
export interface StorageMessage {
  type: MessageType
  unknown1: number
  unknown2: number
  unknown3: number
  unknown4: number
  somesize: number
  length_extended: number
  items: Array<StorageItem>
  kamas: number
  unknown5: number
  unknown6: number
  usedPods: number
  maxPods: number
  unknown8: number
}

export const lookupItemFormat = (b: Buffer) => {
  const formatType = buffer.toNumber(b)
  switch (formatType) {
    case 0x0049: // SOULED MONSTER
      return [
        describeAnkamaNumberFormat('effectId'),
        describeShort('unknown'),
        describeAnkamaNumberFormat('monsterId'),
      ]
    case 0x0046: // CHARACTER STATS
      return [
        describeAnkamaNumberFormat('effectId'), //
        describeAnkamaNumberFormat('effectValue'),
      ]
    case 0x0052: // REWARD BONUS
      return [
        describeAnkamaNumberFormat('effectId'), //
        describeShort('value'),
      ]
    default:
      logger.debug(`Unknown Item FormatType: ${formatType}`)
      throw new Error('Unknown Item FormatType')
  }
}

const ItemEffectFormat: buffer.FormatSpec<ItemEffect> = [
  describeNumber('effectType', 2),
  {
    type: 'format_lookup',
    name: 'itemEffects',
    parameter: {
      off: 'effectType',
      lookup: lookupItemFormat,
    },
  },
]
const StorageItemFormat: buffer.FormatSpec<StorageItem> = [
  describeNumber('unknown1_003f', 2),
  describeAnkamaNumberFormat('itemTypeId'),
  describeArray('effects', ItemEffectFormat, 'short'),
  describeNumber('itemInstanceId', 2),
  describeNumber('unknown1', 2),
  describeAnkamaNumberFormat('stack'),
]

const StorageFormat: buffer.FormatSpec<StorageMessage> = [
  describeNumber('type', 2),
  describeNumber('unknown1', 2), // 0x061a
  describeNumber('unknown2', 4), // 0xffffffff
  describeNumber('unknown3', 2),
  describeNumber('somesize', 2),
  {
    type: 'fixed_length',
    length: 1,
    name: 'length_extended',
    condition: {
      on: 'somesize',
      check: (b: Buffer) => buffer.toNumber(b) > 0x3a00,
    },
  },
  describeArray('items', StorageItemFormat, 'short'),
  describeAnkamaNumberFormat('kamas'),
  describeShort('unknown5'),
  describeNumber('unknown6', 1),
  describeAnkamaNumberFormat('currPods'),
  describeAnkamaNumberFormat('maxPods'),
  describeShort('unknown8'),
]

export const storageParser = (b: Buffer) => {
  const [chunks] = buffer.namedChunks<StorageMessage>(b, StorageFormat)
  accumulateSoulsFromChest(chunks)
}
