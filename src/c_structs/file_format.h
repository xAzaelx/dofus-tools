#include <stddefs.h>

#pragma byte_order(BigEndian)
#define HEADER_LENGTH_BYTES 4
#define BREAK_ARRAY_ON_OFFSET(offset) \
	if (current_offset >= offset)     \
	{                                 \
		$break_array(true);           \
	}

enum D20FieldType : DWORD
{
	INT = 0xFFFFFFFF,
	BOOLEAN = 0xFFFFFFFE,
	STRING = 0xFFFFFFFD, // Unobserved
	DOUBLE = 0xFFFFFFFC, // Unobserved
	I18N = 0xFFFFFFFB,
	UINT = 0xFFFFFFFA, // Unobserved
	LIST = 0xFFFF67,   // Unobserved
	VECTOR_LENGTH = 0xFFFFFF9d,
};

[display(text)]
struct String {
	short length;
	char text[length];
};

public struct I18nFileFormat {

	// ################### StringList ###################
	unsigned int stringListBytes; // excluding header
	var eo_stringList = stringListBytes;
	var stringListLength;
	struct StringList
	{
		unsigned short length;
		char text[length];
		stringListLength = array_index;
		BREAK_ARRAY_ON_OFFSET(eo_stringList)
	} stringList[*];
	$print("stringListLength", stringListLength);

	//################## Pointer List ###################
	unsigned int pointerListBytes; // including header
	var eo_pointerList = eo_stringList + pointerListBytes + HEADER_LENGTH_BYTES;
	var pointerListLength;
	struct PointerList
	{
		unsigned int textId;
		BYTE diacriticFlag;
		unsigned int textStart;
		if (diacriticFlag == 1)
		{
			unsigned int diacriticStart;
		}
		pointerListLength = array_index;
		BREAK_ARRAY_ON_OFFSET(eo_pointerList)
	} pointerList[*];
	$print("pointerListLength", pointerListLength);

	//################## UI String List ###################
	unsigned int uiStringListBytes;
	var eo_uiStringsList = eo_pointerList + uiStringListBytes + HEADER_LENGTH_BYTES;
	var uiStringListLength;
	struct UIStringsList
	{
		unsigned short length;
		char text[length];
		unsigned int maybeId;
		uiStringListLength = array_index;
		BREAK_ARRAY_ON_OFFSET(eo_uiStringsList)
	} uiStringList[*];
	$print("uiStringListLength", uiStringListLength);

	//################## String ID List ###################
	unsigned int stringIdListBytes;
	var eo_stringIdList = eo_uiStringsList + stringIdListBytes + HEADER_LENGTH_BYTES;
	var stringIdListLength;
	struct StringIdList
	{
		unsigned int stringId;
		stringIdListLength = array_index;
		BREAK_ARRAY_ON_OFFSET(eo_stringIdList)
	} stringIdList[*];
	$print("stringIdListLength", stringIdListLength);
};

struct D2O_Field_Descriptor {
	String fieldName;
	D20FieldType fieldType;
	if (fieldType == VECTOR_LENGTH)
	{
		String vector;
		D20FieldType vectorFieldType;
		if (vectorFieldType == VECTOR_LENGTH)
		{
			String vector2;
			D20FieldType vectorFieldType2;
		}
	}
};

struct D2O_Member_Descriptor {
	unsigned int memberId;
	String name;
	String package;
	unsigned int fieldsAmount;
	D2O_Field_Descriptor fieldDescriptor[fieldsAmount];
};

struct D20_Format_Descriptor {
	unsigned int memberAmount;
	D2O_Member_Descriptor members[memberAmount];
};

public struct AchievD20 {

	[color_scheme("HeaderNt")] BYTE D2O_Header[3];
	[color_scheme("Size")] unsigned int achievmentListLength;
	struct AchievmentList {
		unsigned int fixed_00_00_00_01;
		unsigned int id;
		unsigned int nameId;
		unsigned int categoryId;
		unsigned int descriptionId;
		unsigned int iconId;
		unsigned int points;
		unsigned int level;
		unsigned int order;
		BYTE accountLinked;
		unsigned int objectiveIds;
		unsigned int objectives[objectiveIds]; // Vector.<int>
		unsigned int rewardIds;
		unsigned int rewards[rewardIds]; // Vector.<int>

		BREAK_ARRAY_ON_OFFSET(achievmentListLength)
	} achievmentList[*];

	[color_scheme("Size")] unsigned int pointerListLength;
	struct PointerList {
		unsigned int achievmentIdRef;
		unsigned int dataStartPointer;
		BREAK_ARRAY_ON_OFFSET(achievmentListLength + pointerListLength + 4)
	} pointerList[*];

	D20_Format_Descriptor d2oFormatDescriptor;

	[color_scheme("Size")] unsigned int endListLen;
	struct EndList {
		[color_scheme("Characteristics")] unsigned int someInt[11];
	} endList[*];
};


struct AchievmentRewardList {
	unsigned int fixed_00_00_00_01;
	unsigned int id;
	unsigned int achievementId;
	unsigned short criteriaLen;
	char criteria[criteriaLen];
	double kamasRatio;
	double experienceRatio;
	bool kamasScaleWithPlayerLevel;
	unsigned int itemsRewardLen;
	unsigned int itemsReward[itemsRewardLen];
	unsigned int itemsQuantityRewardLen;
	unsigned int itemsQuantityReward[itemsQuantityRewardLen];
	unsigned int emotesRewardLen;
	unsigned int emotesReward[emotesRewardLen];
	unsigned int spellsRewardLen;
	unsigned int spellsReward[spellsRewardLen];
	unsigned int titlesRewardLen;
	unsigned int titlesReward[titlesRewardLen];
	unsigned int ornamentsRewardLen;
	unsigned int ornamentsReward[ornamentsRewardLen];
};	
struct AchievmentCategoryList {
	unsigned int fixed_00_00_00_01;
	unsigned int id;
	unsigned int nameId;
	unsigned int parentId;
	unsigned short iconLen;
	char icon[iconLen];
	unsigned int order;
	unsigned short colorLen;
	char color[colorLen];
	unsigned int achievmentIdsLen;
	unsigned int achievmentIds[achievmentIdsLen];
	unsigned short visibilityCriterionLen;
	char visibilityCriterion[visibilityCriterionLen];
};	
struct AreasList {
	unsigned int fixed_00_00_00_01;
	unsigned int id;
	unsigned int nameId;
	unsigned int superAreaId;
	bool containHouses;
	bool containPaddacks;
	int len;
	struct Rect {
		int x;
		int y;
		int width;
		int height;
	} bounds[len];
	unsigned int worldmapId;
	bool hasWorldMap;
};	
struct IntVector {
	short length; // ! Not sure thou
	int number;
};

struct EffectInstance {
	int member;
	int effectUid;
	int effectId;
	int order;
	int targetId;
	String targetMask;
	int duration;
	int random;
	int group;
	bool visibleInTooltip;
	bool visibleInBuffUi;
	bool visibleInFightLog;
	bool visibleOnTerrain;
	bool forClientOnly;
	int dispellable;
	[color_scheme("Size")]String rawZone;
	int delay;
	String triggers;
	int effectElement;
	int spellId;
};
struct EffectInstanceInteger {
	int member;
	String targetMask;
	bool visibleInBuffUi;
	bool visibleInFightLog;
	int targetId;
	int effectElement;
	int effectUid;
	int dispellable;
	String triggers;
	int spellId;
	int duration;
	int random;
	int effectId;
	int delay;
	bool visibleOnTerrain;
	bool visibleInTooltip;
	String rawZone;
	bool forClientOnly;
	int value;
	int order;
	int group;
};
struct EffectInstanceDice {
	int member;
	String targetMask;
	int diceNum;
	bool visibleInBuffUi;
	bool visibleInFightLog;
	int targetId;
	int effectElement;
	int effectUid;
	int dispellable;
	String triggers;
	int spellId;
	int duration;
	int random;
	int effectId;
	int delay;
	int diceSide;
	bool visibleOnTerrain;
	bool visibleInTooltip;
	String rawZone;
	bool forClientOnly;
	int value;
	int order;
	int group;
};
struct ItemSetsList {
	struct ItemSet {
		int member;
		int id;
		int itemsLen;
		int items[itemsLen];
		int nameId;
		bool bonusIsSecret;
		int effectsLen;
		struct Effects {
			// int effectsInstLen;
			EffectInstance instances;
		} effects[effectsLen];
	} itemSets;
};	
public struct ManualItemSetsList {
	struct ItemSet {
		int member;
		int id;
		int itemsLen;
		int items[itemsLen];
		int nameId;
		bool bonusIsSecret;
		int effectsLen;

		struct SetBuff {
			int setBuffLines;
			struct Test {
				int x01;
				if (x01 != -1431655766)	{
					short x02;
					int value;			// is correct but maybe wrong
					bool visibleInBuffUi;			// is correct but maybe wrong name
					bool visibleInFightLog;			// is correct but maybe wrong name
					int targetId;
					int effectElement;
					int effectUid;
					int dispellable;
					short x02;
					int ffs2;
					int ints[2];
					int effectId;  // Correct
					short x05;
					int ints2[2];
					short len;
					char text[len];
					bool some;
					int ints3[3];
					// short x02;
					// int value;			// is correct but maybe wrong
					// BYTE zeros[6];
					// int ffs;
					// int x02;
					// int x03;
					// short x02;
					// int ffs2;
					// int ints[2];
					// int effectId;  // Correct
					// short x05;
					// int ints2[2];
					// short len;
					// char text[len];
					// bool some;
					// int ints3[3];
				}
				
			} setBuffLines[setBuffLines];
		} setBuffs[effectsLen];
		// struct Effects {
		// 	// int effectsInstLen;
		// 	EffectInstance instances;
		// } effects[effectsLen];
	} itemSets;
};	






public struct Generic_D20 {

	BYTE D2O_Header[3];
	 unsigned int DataLen1;
	var eo_dataList = DataLen1;
	// BYTE Data[eo_dataList - 4 - 3];
	struct Data {
		ManualItemSetsList area;
		BREAK_ARRAY_ON_OFFSET(eo_dataList)
	} data[*];

	unsigned int pointerListLength;
	var eo_pointerList = current_offset + pointerListLength;
	struct PointerList {
		unsigned int dataIdRef;
		unsigned int dataStartPointer;
		BREAK_ARRAY_ON_OFFSET(eo_pointerList)
	} pointerList[*];
	
	D20_Format_Descriptor d2oFormatDescriptor;

	unsigned int endListDescriptorLen;
	var eo_endListDesc = current_offset + endListDescriptorLen;
	var itemCount = 0;
	struct EndListDescriptor {
		String fieldName;
		unsigned int dataStartPointer;
		D20FieldType fieldType;
		unsigned int maybeFileRef;
		itemCount = itemCount+1;
		BREAK_ARRAY_ON_OFFSET(eo_endListDesc)
	} endListDescriptor[*];

	struct EndList {
		[color_scheme("Size")] unsigned int endListLen;
		for (var i = 0; i < itemCount; i = i + 1) {
			var len = 0;
			if(i == itemCount - 1) {
				len = endListLen - endListDescriptor[i].dataStartPointer;
			}
			else {
				len = endListDescriptor[i+1].dataStartPointer - 1 - endListDescriptor[i].dataStartPointer;
			}
			$print("fieldName", endListDescriptor[i].fieldName.text);
			BYTE data[len];
		}
	} endList;

};
