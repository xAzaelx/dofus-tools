#include <stddefs.h>

#pragma byte_order(BigEndian)

[display(text)]
struct String {
	BYTE length;
	char text[length];
};

struct Headers {
    struct Ethernet {
        BYTE dst_addr[6];
        BYTE src_addr[6];
        BYTE type[2];
    } eth;
    struct IP {
        BYTE hdr_len;
        bool ecn;
        short len;
        short id;
        short frag_offset;
        bool ttl;
        bool proto;
        short checksum;
        int src;
        int dst;
    } ip;
    struct TCP {
        short src_port; 
        short dst_port; 
        int seq_raw;
        int ack_raw;
        bool flags[2];
        short window_size_value; 
        short checksum; 
        short urgent_pointer; 
    } tcp;
}; 

struct ChatMessageData {
    BYTE type[2];
    BYTE totalLength;
    BYTE channel;
    BYTE fix1;
    String msg;
    int timestamp;
    short fix2;
    double unknown1;
    BYTE fix3;
    double unknown2;
    String name;
    BYTE unknown3[6];
};
struct MessageItemData {
    short itemAmount;
    struct ItemsData {
        int itemId;
        short statsAmount;
        struct StatsData {
            short fix;
            short statId;
            short value;
        } stats[statsAmount];
    } items[itemAmount];
};

public struct ChatMesage {
    Headers headers;
    ChatMessageData message;
};
public struct ChatMesageWitItems {
    Headers headers;
    ChatMessageData message;
    MessageItemData items;
};


[display(anfValue)]
struct ANF {
    var len = 0;
    struct ANF_ARR {
        BYTE x;
        len = len + 1;
        if (x < 128)
            $break_array(true);
    } c[*];
    var anfValue = 1;
    for(var i = len-1; i >= 0; i=i-1) {
        anfValue = (anfValue-1)*128+element(c[i],0);
    }

};

struct IET_SOUL {
    ANF effectId;
    short unknown1;
    ANF monsterId;
};
struct IET_STATS {
    ANF effectId;
    ANF effectValue;
};
struct IET_REWARD {
    ANF effectId;
    short value;
};

struct InvItemEffect {
    short effectTypeId;
    switch (effectTypeId) {
        case 73:
            IET_SOUL effect;
            break;
        case 70:
            IET_STATS effect;
            break;
        case 82:
            IET_REWARD effect;
            break;
        default:
            $assert("Invalid file");
      }
};

public struct InventoryItems {
    int start[2];
    [color_scheme("Size")] int maybeSize;
    if(maybeSize > 14847) {
        BYTE extendedSize;
    }
    short itemsLen;
    struct InvItem {
        short fix_003f;
        ANF itemTypeId;
        short effectLen;
        InvItemEffect invItemEffects[effectLen];
        short maybeItemInstanceId;
        short something;
        ANF stack;       
    } invItems[itemsLen];
    ANF kamas;
    short unknown5;
    BYTE unknown6;
    ANF currPods;
    ANF maxPods;
    short unknown8;

};

public struct Whatever {
    BYTE xxx[68];
    [color_scheme("Size")] short Money;
};