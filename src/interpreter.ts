import * as buffer from './utils/buffer'
import { chatParser } from './parser/chat-parser'
import { MessageType } from './parser/message.types'
import { storageParser } from './parser/storage-parser'

export const interpret = (b: Buffer) => {
  if (b.length >= 2) {
    const type = buffer.toNumber(b.subarray(0, 2))
    switch (type) {
      case MessageType.StandardChatMessage:
      case MessageType.ItemChatMessage:
        chatParser(b)
        break
      // case MessageType.MessageFollowingType: {
      //   const newBuffer = b.subarray(2, b.length)
      //   interpret(newBuffer)
      //   break
      // }
      // case MessageType.TODO_4IntThenMessage: {
      //   const newBuffer = b.subarray(4 * 4, b.length)
      //   interpret(newBuffer)
      //   break
      // }
      case MessageType.StorageMessage:
        storageParser(b)
        break
      default:
        console.log('Unknown Message Type: ' + type.toString(16))
        return
    }
  } else {
    // console.log('Buffer empty - nothing to interpret')
  }
}
