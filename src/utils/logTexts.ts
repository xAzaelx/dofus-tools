const chalk = require('chalk')
const log = console.log
import * as util from 'util'
import { createLogger, format, transports } from 'winston'
import { consoleFormat } from 'winston-console-format'

export const logger = createLogger({
  level: 'silly',
  format: format.combine(
    format.timestamp(),
    format.ms(),
    format.errors({ stack: true }),
    format.splat(),
    format.json()
  ),
  defaultMeta: { service: 'Test' },
  transports: [
    new transports.Console({
      format: format.combine(
        format.colorize({ all: true }),
        format.padLevels(),
        consoleFormat({
          showMeta: true,
          metaStrip: ['timestamp', 'service'],
          inspectOptions: {
            depth: Infinity,
            colors: true,
            maxArrayLength: Infinity,
            breakLength: 120,
            compact: Infinity,
          },
        })
      ),
    }),
  ],
})

export const notEthernet = () => log(chalk.redBright(`Link Type is not ETHERNET`))

export const ignoringUDP = () => log(chalk.redBright(`Getting UDP message - ignoring for now`))

export const unsupportedIPv4Protocol = (protocol: string) =>
  log(chalk.redBright(`Unsupported IPv4 protocol: ${protocol}`))

export const unsupportedEthertype = (type: string) =>
  log(chalk.redBright(`Unsupported Ethertype: ${type}`))

export const logPath = (text: string) => chalk.blueBright(text)
export const secInit = chalk.green('|')
export const check = chalk.greenBright.bold('\u2713')
export const warn = chalk.yellowBright.bold('\u26A0')
export const cross = chalk.redBright.bold('\u2717')
