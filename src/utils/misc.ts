import * as fs from 'fs'
import * as buffer from './buffer'

export const openJsonFile = (file: string) => {
  const filecontent = fs.readFileSync(file)
  return JSON.parse(buffer.toText(filecontent))
}

export const prettyJson = (json: any) => JSON.stringify(json, null, 4)

export const jsonToInterfaceString = (name: string, json: any) => {
  const fields = JSON.stringify(json, null, 4).replace(/\"/g, '').replace(/,/g, '')
  return `export interface ${name} ${fields}`
}
