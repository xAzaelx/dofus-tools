const chalk = require('chalk')
import { logger } from './logTexts'

export const toNumber = (hexArr: Buffer) => hexArr.reduce((acc, curr) => (acc << 8) | curr)

export const toText = (buffer: Buffer) => buffer.toString('utf-8')

export const toHexString = (buffer: Buffer) => {
  let hexString = buffer.toString('hex')
  hexString = hexString.length > 24 ? hexString.substr(0, 24 - 1) + ' ...' : hexString
  return '0x' + hexString
}

export const split = (buffer: Buffer, len: number): [Buffer, Buffer] => {
  const left = buffer.slice(0, len)
  const right = buffer.slice(len, buffer.length)
  return [left, right]
}

/**
 *
 * @param buffer The buffer which should be chunked
 * @param chunkLengths array with chunk lengths
 */
export function chunk<TItem extends unknown[]>(buffer: [...TItem], chunkLengths: number[]) {
  let chunks = []
  let currIndex = 0
  chunkLengths.forEach((chunkSize) => {
    const chunk = buffer.slice(currIndex, currIndex + chunkSize)
    currIndex += chunkSize
    chunks.push(chunk)
  })
  const restChunk = buffer.slice(currIndex, buffer.length)
  chunks.push(restChunk)
  return chunks
}

interface GenericEntry<T> {
  name: keyof T
  debug?: boolean
}

export interface FixedEntry<Iface> extends GenericEntry<Iface> {
  type: 'fixed_length'
  transform?: (buffer: Buffer) => any
  length: number
  condition?: {
    on: keyof Iface
    check: (buffer: Buffer) => boolean
  }
}
export interface VariableEntry<Iface> extends GenericEntry<Iface> {
  type: 'variable_length'
  transform?: (buffer: Buffer) => any
  parameter: {
    off: keyof Iface
    transform: (b: Buffer) => number
  }
}
export interface FormatLookupEntry<Iface> extends GenericEntry<Iface> {
  type: 'format_lookup'
  parameter: {
    off: keyof Iface
    lookup: (b: Buffer) => FormatSpec<any> | undefined
  }
}
export interface ChunkEntry<Iface, T> extends GenericEntry<Iface> {
  type: 'chunk'
  format: FormatSpec<T>
  transform?: (obj: T) => any
  condition?: {
    on: keyof Iface
    check: (buffer: Buffer) => boolean
  }
}
export interface FixedArrayEntry<Iface> extends GenericEntry<Iface> {
  type: 'fixed_array'
  amount: number
  format: FormatSpec<any>
  transform?: (obj: any) => any
  condition?: {
    on: keyof Iface
    check: (buffer: Buffer) => boolean
  }
}
export interface VariableArrayEntry<Iface> extends GenericEntry<Iface> {
  type: 'variable_array'
  format: FormatSpec<any>
  transform?: (obj: any) => any
  parameter: {
    off: keyof Iface
    transform: (b: Buffer) => number
  }
}
export interface TakeUntilEntry<Iface> extends GenericEntry<Iface> {
  type: 'take_until'
  stopCondition: (b: Buffer) => boolean
  transform: (b: Buffer) => any
}

function checkForDependency<T, K extends keyof T>(buffArr: T, dependency: K, dependent: K) {
  const dp = buffArr[dependency]
  if (dp === undefined) {
    throw new Error(
      `Dependency "${dependency}" does not occur before "${dependent}.  
      There is likely something wrong with the FormatSpecification"`
    )
  }
}

export type FormatEntries<T> =
  | FixedEntry<T>
  | VariableEntry<T>
  | ChunkEntry<T, any>
  | FormatLookupEntry<T>
  | FixedArrayEntry<T>
  | VariableArrayEntry<T>
  | TakeUntilEntry<T>
export type FormatSpec<T> = FormatEntries<T>[]

export function namedChunks<T>(buffer: Buffer, format: FormatSpec<T>): [T, Buffer, number] {
  let buffers = {} as { [key in keyof T]: Buffer }
  let displayValues = {} as { [key in keyof T]: any }
  let currentPos = 0
  try {
    format.forEach((entry) => {
      if (entry.debug) {
        logger.debug('# --------- DEBUG START ---------')
        logger.debug('Debugging for entry', entry)
        logger.debug('Current position: ' + currentPos)
        logger.debug('Current displayValues', displayValues)
        logger.debug('Buffer ' + buffer.subarray(0, 10).toString('hex'))
        logger.debug('# --------- PROCESSING ----------')
      }
      switch (entry.type) {
        case 'fixed_length': {
          if (entry.condition) {
            checkForDependency(buffers, entry.condition.on, entry.name)
            if (!entry.condition.check(buffers[entry.condition.on])) return
          }
          const selection = buffer.slice(currentPos, currentPos + entry.length)
          currentPos += selection.length
          if (selection.length > 0) {
            buffers[entry.name] = selection
            displayValues[entry.name] = entry.transform
              ? entry.transform(selection)
              : toHexString(selection)
          }
          break
        }
        case 'variable_length': {
          checkForDependency(buffers, entry.parameter.off, entry.name)
          const selectionLength = entry.parameter.transform(buffers[entry.parameter.off])
          const selection = buffer.slice(currentPos, currentPos + selectionLength)
          currentPos += selection.length
          if (selection.length > 0) {
            buffers[entry.name] = selection
            displayValues[entry.name] = entry.transform
              ? entry.transform(selection)
              : toHexString(selection)
          }
          break
        }
        case 'format_lookup': {
          checkForDependency(buffers, entry.parameter.off, entry.name)
          const format = entry.parameter.lookup(buffers[entry.parameter.off])
          if (!format) {
            // assuming empty field
            return
          }
          const restBuffer = buffer.slice(currentPos, buffer.length)
          const [myChunk, rest, endPos] = namedChunks(restBuffer, [
            {
              type: 'chunk',
              format,
              name: entry.name,
            },
          ])
          displayValues[entry.name] = myChunk[entry.name]
          currentPos += endPos
          return
        }
        case 'chunk': {
          if (entry.condition) {
            checkForDependency(buffers, entry.condition.on, entry.name)
            if (!entry.condition.check(buffers[entry.condition.on])) return
          }
          const restBuffer = buffer.slice(currentPos, buffer.length)
          const [myChunk, rest, endPos] = namedChunks(restBuffer, entry.format)
          displayValues[entry.name] = entry.transform ? entry.transform(myChunk) : myChunk
          currentPos += endPos
          return
        }
        case 'fixed_array': {
          if (entry.condition) {
            checkForDependency(buffers, entry.condition.on, entry.name)
            if (!entry.condition.check(buffers[entry.condition.on])) return
          }
          let bufferLength = currentPos
          let restBuffer = buffer.slice(currentPos, buffer.length)
          let arr = []
          for (let i = 0; i < entry.amount; i++) {
            const [myChunk, rest, endPos] = namedChunks(restBuffer, entry.format)
            arr.push(entry.transform ? entry.transform(myChunk) : myChunk)
            bufferLength += endPos
            restBuffer = rest
          }
          buffers[entry.name] = buffer.slice(currentPos, bufferLength)
          currentPos = bufferLength
          displayValues[entry.name] = arr
          break
        }
        case 'variable_array': {
          checkForDependency(buffers, entry.parameter.off, entry.name)
          const iterations = entry.parameter.transform(buffers[entry.parameter.off])
          let bufferLength = currentPos
          let restBuffer = buffer.slice(currentPos, buffer.length)
          let arr = []
          for (let i = 0; i < iterations; i++) {
            const [myChunk, rest, endPos] = namedChunks(restBuffer, entry.format)
            arr.push(entry.transform ? entry.transform(myChunk) : myChunk)
            bufferLength += endPos
            restBuffer = rest
          }
          buffers[entry.name] = buffer.slice(currentPos, bufferLength)
          currentPos = bufferLength
          displayValues[entry.name] = arr
          break
        }
        case 'take_until': {
          let bufferLength = 1
          let takeBuffer = buffer.slice(currentPos, currentPos + bufferLength)
          while (entry.stopCondition(takeBuffer) === false) {
            bufferLength++
            takeBuffer = buffer.slice(currentPos, currentPos + bufferLength)
          }
          buffers[entry.name] = takeBuffer
          currentPos += bufferLength
          displayValues[entry.name] = entry.transform(takeBuffer)
          break
        }
        default:
          console.error('FormatSpec not supported: ', entry)
          throw new Error('FormatSpec not supported: ' + JSON.stringify(entry))
      }
      if (entry.debug) {
        logger.debug('# Current displayValues', displayValues)
        logger.debug('# Current position', currentPos)
        logger.debug('# ---------- DEBUG END ----------')
      }
    })
  } catch (e) {
    console.error(e)
    throw e
  }
  const restChunk = buffer.slice(currentPos, buffer.length)
  return [displayValues, restChunk, currentPos]
}

export function recurseNamedChunks<T>(
  buffer: Buffer,
  format: FormatSpec<T>,
  transform?: (c: T) => any
): T[] {
  let arr: T[] = []
  let workingOnBuffer = buffer
  while (workingOnBuffer.length > 0) {
    let [chunks, rest] = namedChunks<T>(workingOnBuffer, format)
    workingOnBuffer = rest
    arr.push(transform ? transform(chunks) : chunks)
  }
  return arr
}
