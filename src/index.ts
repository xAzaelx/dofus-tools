import * as zlib from 'zlib'
import * as fs from 'fs'
import chalk from 'chalk'
import * as interpreter from './interpreter'
import { parseGameFiles } from './parser/game-files/parse-game-files'
import { startCapturing } from './package-capture'
import * as d2o from '../parsed/data/d2o/interfaces'
import * as buffer from './utils/buffer'
import { parseEternalHarvestAsCsv } from './plugins/eternal-harvest'

console.log(
  chalk.greenBright(`
    ____        ____              ______     __ 
   / __ \\____  / __/_  _______   / ____/  __/ /_
  / / / / __ \\/ /_/ / / / ___/  / __/ | |/_/ __/
 / /_/ / /_/ / __/ /_/ (__  )  / /____>  </ /_  
/_____/\\____/_/  \\__,_/____/  /_____/_/|_|\\__/  
                                                                                        
`)
)

parseEternalHarvestAsCsv()

parseGameFiles()
startCapturing(interpreter.interpret)
