export const ANKAMA_FOLDER = process.env.LOCALAPPDATA + '/Ankama'
export const DOFUS_FOLDER = ANKAMA_FOLDER + '/zaap/dofus'

export const I18N_IN = `${DOFUS_FOLDER}/data/i18n/`
export const D2O_IN = `${DOFUS_FOLDER}/data/common/`
export const PARSED_FOLDER = `parsed`
export const GENERATED_FOLDER = `generated`
export const DATA_FOLDER = `${PARSED_FOLDER}/data`
export const ARCHIVE_FOLDER = `${PARSED_FOLDER}/archive`
export const I18N_OUT = `${DATA_FOLDER}/i18n`
export const D2O_OUT = `${DATA_FOLDER}/d2o`
