import * as fs from 'fs'
import { getData, getI18n } from '../memory'
import { GENERATED_FOLDER } from '../constants'
import { StorageMessage } from '../parser/storage-parser'

const CSV_FILE = 'EternalHarvestList.csv'
const ACC_OCHRE_SOULS = 'OchreSouls.csv'
const ACC_RANDOM_SOULS = 'RandomSouls.csv'

export const parseEternalHarvestAsCsv = () => {
  let csvAcc = ''
  const EternalHarvest = getData('Quests')[439]
  const QuestSteps = getData('QuestSteps')
  const QuestObjectives = getData('QuestObjectives')
  const Monsters = getData('Monsters')

  EternalHarvest.stepIds.forEach((stepId, index) => {
    const objectiveIds = QuestSteps[stepId].objectiveIds
    if (index < 34) {
      objectiveIds.forEach((objectiveId) => {
        const objective = QuestObjectives[objectiveId]
        const monsterId = objective.parameters.parameter1
        const monster = Monsters[monsterId]
        const name = getI18n(monster.nameId)
        csvAcc += `${index + 1},${monster.id},${name}\n`
      })
    }
  })
  fs.mkdirSync(GENERATED_FOLDER, { recursive: true })
  fs.writeFileSync(`${GENERATED_FOLDER}/${CSV_FILE}`, csvAcc)
}

interface EH_SoulAccumulator {
  id: string
  step: string
  name: string
  count: number
}
type AccumulatedSoulList = Record<string, EH_SoulAccumulator>
export const eternalHarvestCsvToJsObject = (): AccumulatedSoulList => {
  try {
    const data = fs.readFileSync(`${GENERATED_FOLDER}/${CSV_FILE}`).toString('utf-8')
    const lines = data.split('\n')
    const accumulatedJson: AccumulatedSoulList = {}
    lines.forEach((line) => {
      const [step, id, name] = line.split(',')
      accumulatedJson[id] = { step, name, id, count: 0 }
    })
    return accumulatedJson
  } catch (e) {
    parseEternalHarvestAsCsv()
    return eternalHarvestCsvToJsObject()
  }
}

/**
 * Use this function if you access a Storage containing the Soulstones
 * TODO: Accumulate Stone Instance Ids
 * TODO: Make sure only Soulsstone will be taken into account
 * TODO: Make sure only Souleffects will be taken into account (ignore reward bonus f.i.)
 */
export const accumulateSoulsFromChest = (chunks: StorageMessage) => {
  const ochreSoulAccumulator = eternalHarvestCsvToJsObject()
  const randomSoulAccumulator: Record<string, Omit<EH_SoulAccumulator, 'step'>> = {}
  const Monsters = getData('Monsters')
  chunks.items.forEach((item) => {
    item.effects.forEach((effect) => {
      if (effect.itemEffects.monsterId) {
        const id = effect.itemEffects.monsterId
        if (ochreSoulAccumulator[id] === undefined && randomSoulAccumulator[id] === undefined) {
          const monster = Monsters[id]
          randomSoulAccumulator[id] = { id, name: getI18n(monster.nameId), count: 1 }
        } else if (randomSoulAccumulator[id]) {
          randomSoulAccumulator[id].count += 1
        } else {
          ochreSoulAccumulator[id].count += 1
        }
      }
    })
  })

  const accumulatedOchreSoulsCSV = Object.keys(ochreSoulAccumulator).reduce((acc, monsterId) => {
    const { id, name, step, count } = ochreSoulAccumulator[monsterId]
    return acc + `${step},${id},${name},${count}\n`
  }, '')
  const accumulatedRandomSoulsCSV = Object.keys(randomSoulAccumulator).reduce((acc, monsterId) => {
    const { id, name, count } = randomSoulAccumulator[monsterId]
    return acc + `${id},${name},${count}\n`
  }, '')
  fs.mkdirSync(GENERATED_FOLDER, { recursive: true })
  fs.writeFileSync(`${GENERATED_FOLDER}/${ACC_OCHRE_SOULS}`, accumulatedOchreSoulsCSV)
  fs.writeFileSync(`${GENERATED_FOLDER}/${ACC_RANDOM_SOULS}`, accumulatedRandomSoulsCSV)
}
