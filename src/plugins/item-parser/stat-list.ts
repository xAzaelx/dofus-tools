const stats = {
  0x6f: 'AP',

  0x76: 'Str',
  0x77: 'Agi',
  0x7b: 'Cha',
  0x7c: 'Wis',
  0x7d: 'Vit',
  0x7e: 'Int',

  0x8a01: 'Power',

  0xa303: '-Crit Damage',
  0xa803: 'Fire/str Damage',
  0xa603: 'Fire/str Damage',
  0xac03: 'Air Damage',
  0xaa03: 'Water Damage',
  0xae03: 'Neutral Damage',
  // xx 03 Crit Damage
  // xx 03 Pushback Damage
  // xx 03 Trap Damage

  0xb001: 'PP',
  0xb201: 'Heals',
  0xb601: 'Summon',

  0xa101: 'MP Parry',
  0xa001: 'AP Parry',
  0x9a03: 'AP Reduction',
  0x9c03: 'MP Reducation',
  0x73: 'Crit',
  0x74: '-Range',
  0x75: 'Range',

  0xf005: 'Dodge',
  0xf105: 'Lock',

  0xd201: '%Earth Res',
  0xd301: '%Water Res',
  0xd401: '%Air Res',
  0xd501: '%Fire Res',
  0xd601: '%Neutral Res',
}
