
Vip-Specific
00 00 00 7e 06 16 End of Chat Message

// Q: 
  - Exo?
  - Values > 255  (Overflow Indicator)
  - Negative Stats (Other Stat ID)
  - Maged By (004a stat indicator)
  - Sink (included in posted image?)


98 09 ed Afterimage of -> ItemTypeId

9b (09) Compatible 09 with Hat (... 10 bc 9d 2a 01)
  10 With Hat
  52 With Shield

00 01 
  00 3f ef 6d 00 0b 
    00 46 7d 8a 03 
    00 46 8a 01 48 
    00 46 7c 2f 
    00 46 ae 03 12 
    00 46 f1 05 0d 
    00 46 b2 01 0b 
    00 46 b0 01 0b 
    00 46 a0 01 0a 
    00 46 6f 01 
    00 46 a3 03 14 
    00 4a d9 07 
    00 06 47 61 64 2d 59 61 de ab 96 15 01

00 02 
  00 3f e5 8c 01 00 0e 
    00 46 7d c7 02 
    00 46 76 42 
    00 46 77 40 
    00 46 7c 21 
    00 46 ae 03 0b 
    00 46 a6 03 0b 
    00 46 ac 03 0b 
    00 46 d5 01 0a 
    00 46 a1 01 08 
    00 46 f1 05 06 
    00 46 9a 03 04 
    00 46 73 03 
    00 46 d4 01 03 
    00 46 75 01 
    ec f6 ba 0c 01 
  00 3f e5 8c 01 00 0d 00
    46 7d cf 02 
      00 46 76 42 
      00 46 77 40 
      00 46 7c 1e
    
      00 46 ae 03 0b 
      00 46 a6 03 0b 
      00 46 ac 03 0b 00
    46 d5 01 0a 
      00 46 a1 01 08 
      00 46 f1 05 05 
      00 46
    73 04 
      00 46 9a 03 04 
      00 46 75 01 83 de 2e 01

00 07 
  00 3f eb 12 00 01 
    00 46 76 03 d3 d6 c9 24 01 
  00 3f eb 12 00 01 
    00 46 76 03 d3 d6 c9 24 01 
  00 3f eb 12 00 01 
    00 46 76 03 d3 d6 c9 24 01 
  00 3f eb 12 00 01 
    00 46 76 03 d3 d6 c9 24 01 
  00 3f eb 12 00 01 
    00 46 76 03 d3 d6 c9 24 01 
  00 3f eb 12 00 01 
    00 46 76 03 d3 d6 c9 24 01 
  00 3f eb 12 00 01 
    00 46 76 03 d3 d6 c9 24 01 

ITEM LIST Format
-------------------------
|FIX | #Items | Items[] |
| -- |   --   | -xxxxx- |
| 00 |        |         |

ITEM Format
---------------------------------------------------------------
| ISI   | ItemTypeID  | #Stat | Stat[] | ItemInstanceID | IEI |
| xx xx | -- -- -> 00 |  --   | -vvvv- | -- -- -- -- -- | xx  |
| 00 3F |             |       |        |                | 01  |

ISI = ItemStartIndicator
IEI = ItemEndIndicator
SSI = StatStartIndicator

STAT Format
--------------------------------
| SSI   | StatId | StatValue | StatOverflow
| -- -- |   --   |     --    |  --
| 00 46 |        |           |
// StatOverflow indicated values >= 255
// theoretically 01 means 64 ?

// 02 means add 128
// 03 means add 256
// theoretically 04 means 512 ?

 // {recipe,12114} [Recipe: Bearbaric Band] // id 792215 counter 32301
// {chatachievement,1695} [King Dazahk's Brewery] // id 8879 counter 759628