import { ChatMessage, chatParser } from '../../parser/chat-parser'
import moment from 'moment'
import chalk from 'chalk'
import { channelDefinition } from './channels'
import { getData, getI18n, getI18nByKey } from '../../memory'
import { logger } from '../../utils/logTexts'

export const formatMsg = (chat: ChatMessage) => {
  const time = moment.unix(chat.timestamp).format('hh:mm')
  const channel = channelDefinition[chat.channel]
  const channels = getData('ChatChannels')[chat.channel]
  const channelName = getI18n(channels.nameId)
  const prefix = channel?.prefix ?? 'UNKNOWN 0x' + chat.channel.toString(16)
  const color = channel?.color ?? '#ff0000'
  const name = chalk.bold(chat.name)
  let message = chat.msg
  SpecialCharacterMapping.forEach((mapping) => {
    message = message.replace(mapping[0], mapping[1])
  })
  message = insertLinkedData(message)
  return chalk.hex(color)(`${time} ${channelName} | ${name}: ${message}`)
}

const insertLinkedData = (message: string) =>
  message.replace(/{.+?}/g, (match) => {
    const [head, ...tail] = match.substring(1, match.length - 1).split(',')

    switch (head) {
      case 'guild': // e.g. {guild,10571,Serenity}
        return chalk.bold(`[${getI18nByKey(821)}: ${tail[1]}]`) // I18nkey: 821 66373 538396
      case 'alliance': // e.g. {alliance,1687,CREMA}
        return chalk.bold(`[${getI18nByKey(258277)}: ${tail[1]}]`) // I18nkey: 258277 261952 335316
      case 'recipe': // e.g. {recipe,13157}
        let recipeName
        const recipe = getData('Recipes')[parseInt(tail[0])]
        if (recipe) {
          recipeName = getI18n(recipe.resultNameId)
        } else {
          const item = getData('Items')[parseInt(tail[0])]
          recipeName = getI18n(item.nameId)
        }
        return chalk.bold(`[${getI18nByKey(521816)}: ${recipeName}]`) // I18nkey: 521816
      case 'chatmonster': // e.g. {chatmonster,232}
        const monster = getData('Monsters')[parseInt(tail[0])]
        const monsterName = getI18n(monster.nameId)
        return chalk.bold(`[${getI18nByKey(101521)}: ${monsterName}]`) // I18nkey: 101521 631209
      case 'chatachievement': // e.g. {chatachievement,13}
        const achievment = getData('Achievements')[parseInt(tail[0])]
        const achievName = getI18n(achievment.nameId)
        return chalk.bold(`[${getI18nByKey(335333)}: ${achievName}]`) // I18nkey: 335333
      case 'spell': // e.g. {spell,9348,1}
        const spell = getData('Spells')[parseInt(tail[0])]
        const spellName = getI18n(spell.nameId)
        return chalk.bold(`[${getI18nByKey(335358).replace('%1', spellName)}]`) // I18nkey: "335358": "Spell: %1",
      case 'map': // e.g. {map,-78,-41,1,House%20} {map,-29,-60,1,} <- just pos
        const posPrefix = tail[3] ? tail[3].replace(/%20/g, ' ') : ''
        return chalk.bold(`${posPrefix}[${tail[0]},${tail[1]}]`)
      // TODO
      case 'monsterGroup': // e.g. {monsterGroup,-81,-70,1,Dremoan,-20000;0;2x2908x180|2x2895x182}

      default:
        return match
    }
  })

const SpecialCharacterMapping: Array<[RegExp, string]> = [
  [/&amp;amp;/g, '&'],
  [/&amp;#123;/g, '{'],
  [/&amp;#125;/g, '}'],
  [/&lt;/g, '<'],
  [/&gt;/g, '>'],
]
