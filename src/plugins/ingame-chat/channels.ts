export enum Channel {
  General = 0x00,
  Team = 0x01,
  Guild = 0x02,
  Alliance = 0x03,
  Group = 0x04,
  Trade = 0x05,
  Recruitment = 0x06,
  Private = 0x09,
  Kolossium = 0x0d,
  CommunityDE = 0x0e,
}

type ChannelDefinition = {
  [key in Channel]: {
    color: string
    prefix: string
  }
}

export const channelDefinition: ChannelDefinition = {
  [Channel.General]: {
    color: '#ffffff',
    prefix: '/s',
  },
  [Channel.Team]: {
    color: '#ff0058',
    prefix: '/t',
  },
  [Channel.Guild]: {
    color: '#975FFF',
    prefix: '/g',
  },
  [Channel.Alliance]: {
    color: '#ffAD42',
    prefix: '/a',
  },
  [Channel.Group]: {
    color: '#00E4FF',
    prefix: '/p',
  },
  [Channel.Trade]: {
    color: '#B3783E',
    prefix: '/b',
  },
  [Channel.Recruitment]: {
    color: '#E4A0D5',
    prefix: '/r',
  },
  [Channel.Private]: {
    color: '#7EC3FF',
    prefix: '~',
  },
  [Channel.Kolossium]: {
    color: '#F16392',
    prefix: 'KOLO',
  },
  [Channel.CommunityDE]: {
    color: '#63F192',
    prefix: 'CMTY',
  },
}
